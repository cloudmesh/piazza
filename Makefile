UNAME := $(shell uname)

BROWSER=firefox
ifeq ($(UNAME), Darwin)
BROWSER=open
endif
ifeq ($(UNAME), Windows)
BROWSER=/cygdrive/c/Program\ Files\ \(x86\)/Google/Chrome/Application/chrome.exe
endif
ifeq ($(UNAME), CYGWIN_NT-6.3)
BROWSER=/cygdrive/c/Program\ Files\ \(x86\)/Google/Chrome/Application/chrome.exe
endif

doc:
	git log --format='%aN' | sort -u > AUTHORS
	pip install -r requirements-doc.txt
	sphinx-apidoc -f -o docs/source/code cloudmesh_piazza
	cd docs; make html
	# cp -r scripts docs/build/html

simple:
	cd docs; make html

#pex:
#    pex  -r <`pip freeze`  -e cloudmesh_client.shell.cm.main  -o my_virtualenv.pex

watch:
	watchmedo shell-command --patterns="*.rst" --recursive --command='make doc'


test:
	echo $(UNAME)

publish:
	ghp-import -n -p docs/build/html

view:
	$(BROWSER) docs/build/html/index.html

setup:
	python setup.py install

dist: clean
	python setup.py sdist --formats=gztar,zip
	python setup.py bdist
	python setup.py bdist_wheel

upload_test:
	python setup.py	 sdist bdist bdist_wheel upload -r https://testpypi.python.org/pypi

upload:
	python setup.py	 sdist bdist bdist_wheel upload -r https://pypi.python.org/pypi

log:
	gitchangelog | fgrep -v ":dev:" | fgrep -v ":new:" > ChangeLog
	git commit -m "chg: dev: Update ChangeLog" ChangeLog
	git push

# Freeze the requirements using the order specified in the
# unconstrained list.  Some packages' installation requires others to
# be installed, and `pip freeze` outputs in lexicographic order by
# default. A side-effect is that all comments in the parameter to -r
# are kept, so we pipe through egrep to remove them.

freeze:
	echo "Sanity check: this package should not be installed"
	if [ `pip freeze | grep -i cloudmesh-client` ]; then echo "Check failed";  return 1; fi
	pip freeze -r requirements-open.txt | egrep -v '^#' >requirements.txt



######################################################################
# TESTING
######################################################################

pytest:
	make -f Makefile clean
	pip install -r requirements.txt
	python setup.py install
	cm register remote
	py.test tests

nosetest:
	make -f Makefile clean
	pip install -r requirements.txt
	python setup.py install
	cm register remote
	nosetests -v --nocapture tests

######################################################################
# CLEANING
######################################################################

clean:
	rm -rf *.zip
	rm -rf *.egg-info
	rm -rf *.eggs
	rm -rf docs/build
	rm -rf build
	rm -rf dist
	find . -name '__pycache__' -delete
	find . -name '*.pyc' -delete
	rm -rf .tox
	rm -f *.whl

######################################################################
# TAGGING
######################################################################

tag:
	bin/new_version.sh

rmtag:
	python setup.py rmtag
