Piazza provides a free Q&A platform that is often used in class
settings. It is superior to other systems, but has at this time one
major flaw. There is no official mechanism to access piazza through
python or an API. Thus we have started a small project that simplifies
access for us.

This is a prototype and needs many improvements. If you like to work
with us let us know your improvements via pull requests and we add
them. At present this is hosted at gitlab, but we may move soon to github.




Setup from Development
======================================================================

    git clone git@gitlab.com:cloudmesh/piazza.git

or

	git clone https://gitlab.com/cloudmesh/piazza.git

Then

	cd piazza
	python setup.py install
	piazza

Setup
=====

Place a .piazza file in the directory where you like to do that piazza
analysis. It is a yaml file of the following format

	piazza:
	    email: <PUT YOUR EMAIL YOU USE IN PIAZZA HERE>
	    password: <PUT YOUR PASSWORD THAT YOU USE IN PIAZZA HERE>
	    id: <PUT THE ID OF THE PIAZZA PROJECT HERE, NOT YET SUPPORTD CHANGE FOR NOW IN CODE>

Usage
=====

Download all main post (not responses) from a folder where d1 is the
folder name

    piazza list d1

This will create a file called d1.json. Next print the post in html

	piazza convert d1 --html

This will produce a file called d1.html. Next view it with your
browser, on OSX you can say simply

	open d1.html

TODO
====

Lots of things have to be done.

* [ ] update the format to eiminate non ascii characters
* [ ] replace user ids with names
* [ ] add user management
* [ ] consider using piazza_api
* [ ] statistics for posts
* [ ] integrate responses with flag
* [ ] export to latex
* [ ] implement find/search




