# Tim Whitson
# this script is for downloading posts off of Piazza
# requires "requests" package (pip install requests)

import requests, json

# input your username and password for authentication
email = ''
password = ''

class_id = 'irqfvh1ctrg2vt'

folder = 'd1'

def get_aid( content ) : ## could do as json???
    return content.split( '"aid"' )[1].split('"')[1]

# login request to get cookie
print "logging in..."
login_url = 'https://piazza.com/logic/api?method=user.login'
login_data = json.dumps( { 'method': 'user.login', 'params': { 'email': email, 'pass': password } } )

login = requests.post( login_url, data = login_data )

#get class page
print "entering class..."
class_url = 'https://piazza.com/class/' + class_id
r = requests.get( class_url, cookies = login.cookies )

#get aid for feed
aid = get_aid( r.content )

#get posts from folder
print "getting items from folder..."
data = json.dumps( {'method': 'network.filter_feed', 'aid': aid, 'params': { 'nid': class_id, 'filter_folder': folder, 'folder': '1' } } )
r = requests.post( 'https://piazza.com/logic/api', data = data, cookies = login.cookies )
feed = json.loads( r.content )['result']['feed']

for post in feed :
    data = json.dumps({'method': 'content.get', 'aid': 'isl54i94u8sf', 'params': {'cid': post['id'], 'nid': class_id}})
    r = requests.post( 'https://piazza.com/logic/api', data = data, cookies = login.cookies )
    post_json = json.loads( r.content )['result']['history'][0]
    subject = post_json[ 'subject' ] # post subject
    content = post_json[ 'content' ] # post content
    print subject # just to make sure it works

