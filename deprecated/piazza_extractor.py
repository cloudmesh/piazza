# Tim Whitson
# this script is for downloading posts off of Piazza
# requires "requests" package (pip install requests)
# needs: error handling
# optional: gather comments on posts
from __future__ import print_function
import requests, json, getpass, sys

# input your username and password for authentication, use this to get around prompt

class PiazzaExtractor (object):

    def __init__(self):    
        self.class_id = 'irqfvh1ctrg2vt'
        self.api_url = 'https://piazza.com/logic/api'
        self.login_cookie = None

    def authenticate(self, email=None, password=None):
        #get username and password
        self.email = email if email else raw_input('Enter login email: ')
        self.password = password if password else getpass.getpass('Enter your password')

        # login request to get cookie
        print ("logging in...")
        login_data = json.dumps({'method': 'user.login',
                                 'params': {
                                     'email': self.email,
                                     'pass': self.password
                                     }
                                })
        self.login = requests.post(self.api_url, data = login_data)
        print ("LLL",self.login)
        print (self.login.cookies)
    
        print ("Login")
        print ("Email:", self.email)
        print ("Password:", self.password) # for debugging
        print ("Cockie:")
        print (self.login)
        print (dir(self.login))
        print (dir(self.login.text))

    def get_folder_posts(self, folder):
        print ("getting items from folder {folder}...".format(**locals()))
        data = json.dumps({'method': 'network.filter_feed',
                           'params': {
                               'nid': self.class_id,
                               'filter_folder': folder,
                               'folder': '1'
                               }
                            })
        folder_request = requests.post(self.api_url, data = data, cookies = self.login_cookie)
        print (folder_request)
        print (folder_request.content)
        feed = json.loads(folder_request.content)['result']['feed']

        # create array of dicts from post feed
        feed_list = []

        # grabs most recent post edit w/ fields 'uid', 'created',
        # 'subject', and 'content' and add to array
        for index, post in enumerate(feed):
            data = json.dumps({'method': 'content.get',
                               'params': {
                                   'cid': post['id'],
                                   'nid': self.class_id
                                    }
                                })
            r = requests.post(self.api_url,
                              data = data,
                              cookies = self.login_cookie)
            post_json = json.loads( r.content )['result']['history'][0] # get most current edit
            uid = post_json['uid'] # uid of poster
            created = post_json['created'] # post creation/edit date
            subject = post_json['subject'] # post subject
            content = post_json['content'] # post content (html)
            feed_list.append({'uid': uid,
                              'created': created,
                              'subject': subject,
                              'content': content})
            
            # update progress
            sys.stdout.write('\r')
            sys.stdout.write(str(index) + '/' + str(len(feed)))
            sys.stdout.flush()
            
        return json.dumps({'feed': feed_list})

    # save/overwrite file as 'piazza_(class_id)_(folder).json'
    def save_folder_posts(self, folder):
        posts = self.get_folder_posts(folder)
        print ("writing file...")
        f = open("{folder}.json".format(**locals()), 'w+')
        f.write(posts)
        f.close

# example:
#piazza = PiazzaExtractor()
#piazza.authenticate()
#piazza.save_folder_posts(class_id, 'd1')
